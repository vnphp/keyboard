<?php

use Vnphp\Keyboard\Keyboard;

class KeyboardTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Keyboard
     */
    protected $instance;

    protected function setUp()
    {
        parent::setUp();
        $this->instance = new Keyboard();
    }

    public function testCorrectLayout()
    {
        static::assertEquals('media', $this->instance->changeLayout('ьувшф'));
        static::assertEquals('мама', $this->instance->changeLayout('vfvf'));
    }
}
