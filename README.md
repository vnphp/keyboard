# Vnphp Keyboard


[![build status](https://gitlab.com/vnphp/keyboard/badges/master/build.svg)](https://gitlab.com/vnphp/keyboard/commits/master)
[![code quality](https://insight.sensiolabs.com/projects/e2c64b4f-2e2a-4ebf-a6b8-2b36fffd2637/big.png)](https://insight.sensiolabs.com/projects/e2c64b4f-2e2a-4ebf-a6b8-2b36fffd2637)

## Installation 

```
composer require vnphp/keyboard
```

You need to have `git` installed. 

## Usage

### 1. Change keyboard layout
```php
<?php

$keyboard = new \Vnphp\Keyboard\Keyboard();
$keyboard->changeLayout('vfvf'); // мама
$keyboard->changeLayout('ьувшф'); // media
```
